import sys
import threading
import tkinter as tk

import pyttsx3 as tts
from speech_recognition import Recognizer, Microphone
from neuralintents import GenericAssistant


def create_file():
    with open("someFile.txt") as f:
        f.write("HELLO WORLD")


class Assistant:
    def __init__(self):
        self.recognizer = Recognizer()
        self.speaker = tts.init()
        self.speaker.setProperty("rate", 150)

        self.assistant = GenericAssistant("intents.json", intent_methods={
            "file": create_file})
        self.assistant.train_model()

        self.root = tk.Tk()
        self.label = tk.Label(text="🤖")
        self.label.pack()

        threading.Thread(target=self.run_assistant).start()

        self.root.mainloop()

    def run_assistant(self):
        while True:
            try:
                with Microphone() as mic:
                    self.recognizer.adjust_for_ambient_noise(mic, duration=0.2)
                    audio = self.recognizer.listen(mic)

                    text = self.recognizer.recognize_google(audio)
                    text = text.lower()

                    if "hey jake" in text:
                        self.label.config(fg="red")
                        audio = self.recognizer.listen(mic)
                        text = self.recognizer.recognize_google(audio)
                        text = text.lower()
                        if text == "stop":
                            self.speaker.say("Bye")
                            self.speaker.runAndWait()
                            self.speaker.stop()
                            self.root.destroy()
                            sys.exit()
                        else:
                            if text is not None:
                                response = self.assistant.request(text)
                                if response is None:
                                    self.speaker.say(str(response))
                                    self.speaker.runAndWait()
                            self.label.config(fg="black")
            except RuntimeError:
                self.label.config(fg="black")
                continue


if __name__ == '__main__':
    Assistant()
